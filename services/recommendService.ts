import { Knex } from "knex";
import { Comment, AiScore } from "../models";
// import fetch from "node-fetch";
export class RecommendService {
  constructor(private knex:Knex) {}

  async getCommentAI() {
    // get no. of positive
    let posForBrand: any = await this.knex<Comment>("comment")
      .select("brand.name as bdname")
      .count("comment_ai_score.pos as positive")
      .where("comment_ai_score.pos", true)
      .leftJoin("brand", "brand.id", "comment.brand_id")
      .leftJoin("comment_ai_score", "comment_ai_score.comment_id", "comment.id")
      .groupBy("brand.name")
      .orderBy("positive");
    //get no. of negative
    let negForBrand: any = await this.knex<Comment>("comment")
      .select("brand.name as bdname")
      .count("comment_ai_score.pos as negative")
      .where("comment_ai_score.pos", false)
      .leftJoin("brand", "brand.id", "comment.brand_id")
      .leftJoin("comment_ai_score", "comment_ai_score.comment_id", "comment.id")
      .groupBy("brand.name");
    //merge no. of pos and neg
    for (let posForBrands of posForBrand) {
      for (let negForBrands of negForBrand) {

        if (posForBrands.bdname == negForBrands.bdname) {
          // posForBrands.positive = parseInt(posForBrand.positive)
          posForBrands.negative = parseInt(negForBrands.negative);
          posForBrands.point = posForBrands.positive - posForBrands.negative;
        } else if (!posForBrands.negative) {
          posForBrands.negative = 0;
          posForBrands.point = +posForBrands.positive - posForBrands.negative;
        }

      }
    }

    //sorting the return json
    posForBrand.sort(function (a: any, b: any) {
      if (a.point < b.point) {
        return 1;
      } else if (a.point > b.point) {
        return -1;
      }
      return a - b;
    });
    return posForBrand;
  }

  async getCommentDetail() {
    // get avg score from comment ai
    let avgScore: any = await this.knex<AiScore>("comment_ai_score")
      .select("comment_id")
      .avg("score as scores")
      .groupBy("comment_id");
    // get all comment
    let comments: any = await this.knex<Comment>("comment").select("*");
    // get single score
    //merge
    for (let commentss of comments) {
      for (let avgScores of avgScore) {
        if (avgScores.comment_id == commentss.id) {
          commentss.score = avgScores.scores;
          commentss.totalScore = commentss.score * 20 * commentss.marks;
        } else if (!commentss.score) {
          commentss.score = 0.4;
          commentss.totalScore = commentss.score * 20 * commentss.marks;
        }
      }
    }
    //sorting
    comments.sort(function (a: any, b: any) {
      if (a.totalScore < b.totalScore) {
        return 1;
      } else if (a.totalScore > b.totalScore) {
        return -1;
      }
      return a - b;
    });

    const set = new Set();
    const result = comments.filter((item: { name: any }) =>
      !set.has(item.name) ? set.add(item.name) : false
    );
    
    return result;
  }

  // async getCommentSingleScore(){
  //   let scores:any = await this.knex<AiScore>("comment_ai_score").select("*");

  //   return scores;
  // }
}
//   async postCommentAI(smartphoneId: number) {
//     const result = await this.knex<Smartphone>("smartphone").where(
//       "smartphone.id",
//       smartphoneId
//     );
//     return result;
//   }
// }
