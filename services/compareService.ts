import { Knex } from "knex";
// import { CompareController } from "../controllers/CompareController";
import { Brand, Smartphone } from "../models";

export class CompareService {
  constructor(private knex: Knex) {}

  async getBrandIdByName(name: string) {
    let result = await this.knex<Brand>("brand")
      .first()
      .where("brand.name", name);
    return result;
  }

  async getPhoneByBrandId(id: number) {
    let result = await this.knex<Smartphone>("smartphone").where(
      "brand_id",
      id
    );
    return result;
  }

  async getPhoneByPhoneId() {
    let result = await this.knex<Smartphone>("smartphone");
    // console.log("phone", result);
    return result;
  }

  async getPhoneByName(name:string){
    let result = await this.knex.select('smartphone').where('name', name)
    return result
  }
}
