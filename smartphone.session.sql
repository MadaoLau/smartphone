SELECT
    bn.name,
    COUNT(cas.pos)
FROM
    comment
    LEFT JOIN brand as bn ON bn.id = comment.brand_id
    LEFT JOIN comment_ai_score as cas ON cas.comment_id = comment.id
WHERE
    cas.pos = TRUE
GROUP BY
    bn.name,
    cas.pos
ORDER BY
    cas.pos
SELECT
    bn.name,
    json_agg(
        json_build_object(
            'brand',
            bn.name,
            'name',
            (
                SELECT
            )
        )
    ) AS agg
FROM
    comment
    LEFT JOIN brand as bn ON bn.id = comment.brand_id
    LEFT JOIN comment_ai_score as cas ON cas.comment_id = comment.id
WHERE
    cas.pos = TRUE
GROUP BY
    bn.name,
    cas.pos
ORDER BY
    cas.pos



SELECT * FROM comment_ai_score

SELECT comment_id, AVG(score)
FROM comment_ai_score
GROUP BY comment_id
    

SELECT
    cm.name,
    AVG(cas.score) * 100 as scores
FROM comment as cm
    LEFT JOIN comment_ai_score as cas ON cm.id = cas.comment_id
WHERE cas.score IS NOT NULL
GROUP BY
    cm.name
ORDER BY 
    scores DESC




SELECT
    cm.name,
    cas.score
FROM comment as cm
    JOIN comment_ai_score as cas ON cm.id = cas.comment_id
GROUP BY
    cm.name, cas.score