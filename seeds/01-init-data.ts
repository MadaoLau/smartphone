import { Knex } from "knex";
import path from "path";
import xlsx from "xlsx";
import { decode } from "utf8";

interface BrandRow {
  name: string;
}

interface SmartPhoneRow {
  name: string;
  image: string;
  price: number;
  release_date: string;
  network: string;
  os: string;
  cpu: string;
  screen_size: string;
  resolution: string;
  refresh_rate: string;
  front_cam: string;
  main_cam: string;
  ram: string;
  storage: string;
  functions: string;
  quick_charge_rate: string;
  wifi_generation: string;
  bluetooth: string;
  weight: string;
  size: string;
  battery: string;
  ip_code: string;
  brand_id: number;
}

interface CommentRow {
  cm1: string;
  cm2: string;
  cm3: string;
  name: string;
  marks: number;
  brand_id: number;
}

interface AiScoreRow {
  comment: string;
  score: number;
  pos: boolean;
  comment_id: number;
}

function readDataSet<T>(filename: string) {
  const BASE_PATH = path.join(__dirname, "datasets");
  const workbook = xlsx.readFile(path.join(BASE_PATH, filename));
  const data = xlsx.utils.sheet_to_json<T>(workbook.Sheets["Sheet1"]);
  return data;
}

export async function seed(knex: Knex): Promise<void> {
  const brandData = readDataSet<BrandRow>("brand.csv");
  const smartphoneData = readDataSet<SmartPhoneRow>("smartphone_details.csv");
  const commentData = readDataSet<CommentRow>("comment_all.csv");
  let aiScore = readDataSet<AiScoreRow>("allcomment_score.csv");

  for (let ai of aiScore) {
    if (ai.score >= 0.5) {
      ai.pos = true;
    } else {
      ai.pos = false;
    }
  }
  const txn = await knex.transaction();
  try {
    // Deletes ALL existing entries (會自動累積id)
    // await txn("comment").del();
    // await txn("smartphone").del();
    // await txn("brand").del();

    //TRUNCATE: To remove all records from a table (以後seed run都會由id:1開始計, 唔會再自動累積id)
    await txn.raw(/*sql*/ `TRUNCATE comment RESTART IDENTITY CASCADE`);
    await txn.raw(/*sql*/ `TRUNCATE smartphone RESTART IDENTITY CASCADE`);
    await txn.raw(/*sql*/ `TRUNCATE brand RESTART IDENTITY CASCADE`);
    await txn.raw(/*sql*/ `TRUNCATE comment_ai_score RESTART IDENTITY CASCADE`);

    // Insert brand
    const brands = brandData.map((brand) => ({ name: decode(brand.name) }));
    const ids = await txn("brand").insert(brands).returning("*");

    // insert smartphone
    const smartphones = smartphoneData.map((smartphone) => ({
      name: decode(smartphone.name).replace(/                    /g, ""), // 強行整走空格
      image: smartphone.image,
      price: smartphone.price,
      release_date: decode(smartphone.release_date),
      network: smartphone.network,
      os: decode(smartphone.os),
      cpu: decode(smartphone.cpu),
      screen_size: decode(smartphone.screen_size),
      resolution: smartphone.resolution,
      refresh_rate: smartphone.refresh_rate,
      front_cam: decode(smartphone.front_cam),
      main_cam: decode(smartphone.main_cam),
      ram: smartphone.ram,
      storage: smartphone.storage,
      functions: decode(smartphone.functions),
      quick_charge_rate: smartphone.quick_charge_rate,
      wifi_generation: smartphone.wifi_generation,
      bluetooth: smartphone.bluetooth,
      weight: smartphone.weight,
      size: decode(smartphone.size),
      battery: smartphone.battery,
      ip_code: smartphone.ip_code,
      //正常牌子無問題, 奇怪的中文牌子或吃不到ID的直接放一個叫Other牌子的ID
      brand_id: ids.filter(
        (i) =>
          i.name === decode(smartphone.name.toString().split(" ")[0]) ||
          i.name === "Other"
      )[0].id, // 直接抽返brand name出黎
    }));
    await txn("smartphone").insert(smartphones).returning("id");

    // insert comment
    const comments = commentData.map((comment) => ({
      cm1: decode(comment.cm1),
      cm2: decode(comment.cm2),
      cm3: decode(comment.cm3),
      name: decode(comment.name),
      marks: comment.marks,
      brand_id: ids.filter(
        (i) => decode(comment.name).includes(i.name) || i.name == "Other"
      )[0].id,
    }));
    // console.log(`comments: `, comments);
    const cds = await txn("comment").insert(comments).returning("*");

    for (let scores of aiScore) {
      await txn("comment_ai_score").insert([
        {
          comment: decode(scores.comment),
          score: scores.score,
          pos: scores.pos,
          comment_id: cds.filter(
            (i) =>
              decode(scores.comment).includes(i.cm1 || i.cm2 || i.cm3) ||
              i.name == "Other"
          )[0].id,
        },
      ]);
    }

    await txn.commit();
  } catch (err) {
    console.log(err);
    await txn.rollback();
  }
}
