import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    // await knex("table_name").del();

    // Inserts seed entries
    await knex("comment").update({brand_id: 3}).where('name','like','vivo%');
    await knex("comment").update({brand_id: 18}).where('name','like','Asus%');
    await knex("comment").update({brand_id: 16}).where('name','like','iPhone%');
    await knex("comment").update({brand_id: 16}).where('name','like','iPad%');
    await knex("comment").update({brand_id: 8}).where('name','like','HUAWEI%');
    await knex("comment").update({brand_id: 6}).where('name','like','%米%');
    await knex("comment").update({brand_id: 6}).where('name','like','Redmi%');
    await knex("comment").update({brand_id: 6}).where('name','like','OPPO%');
    

};
