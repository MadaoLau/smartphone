import { Application } from "express";
import { CompareController } from "./controllers/compareController";
import { RecommendController } from "./controllers/recommendController";

export interface Brand {
  id: number;
  name: string;
}

export interface Smartphone {
  id: number;
  name: string;
  image: string;
  price: number;
  release_date: string;
  network: string;
  os: string;
  cpu: string;
  screen_size: string;
  resolution: string;
  refresh_rate: string;
  front_cam: string;
  main_cam: string;
  ram: string;
  storage: string;
  functions: string;
  quick_charge_rate: string;
  wifi_generation: string;
  bluetooth: string;
  weight: string;
  size: string;
  battery: string;
  ip_code: string;
  brand_id: number;
}

export interface Comment {
  score: any;
  id: number;
  cm1: string;
  cm2: string;
  cm3: string;
  name: string;
  marks: number;
  brand_id: number;
}

export interface AiScore {
  id: number;
  comment: string;
  score: number;
  pos: boolean;
  comment_id: number;
}

export interface RouterOptions {
  app: Application;
  recommendController: RecommendController;
  compareController: CompareController;
}
