import express from "express";
import { compareRoutes } from "./compareRoutes";
import { recommendRoutes } from "./recommendRoutes";

export const routes = express.Router();

routes.use("/compare", compareRoutes);
routes.use("/recommends", recommendRoutes);
