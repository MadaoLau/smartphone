import express from "express";
import { knex } from "../server";
import { RecommendService } from "../services/recommendService";
import { RecommendController } from "../controllers/recommendController";

const recommendService = new RecommendService(knex);
const recommendController = new RecommendController(recommendService);

export const recommendRoutes = express.Router();

recommendRoutes.get("/ai", recommendController.getCommentAI);
recommendRoutes.get("/", recommendController.getCommentDetail);
recommendRoutes.post("/predict", recommendController.getCommentAndPredict);

// recommendRoutes.get("/score", recommendController.getCommentScore);
// recommendRoutes.post("/", recommendController.chooseScore);
