import express from "express";
import { knex } from "../server";
import { CompareService } from "../services/compareService";
import { CompareController } from "../controllers/compareController";

const compareService = new CompareService(knex);
const compareController = new CompareController(compareService);

export const compareRoutes = express.Router();

compareRoutes.get("/", compareController.getOnePhoneByBrandId);
compareRoutes.post("/", compareController.getPhoneByBrandId);
