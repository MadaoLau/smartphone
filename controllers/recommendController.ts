import { RecommendService } from "../services/recommendService";
import { Request, Response } from "express";
import axios from "axios";

//import { scrapHKprice } from "../scrap-Dc/hkprice";

export class RecommendController {
  constructor(private recommendService: RecommendService) {}

  getCommentAI = async (req: Request, res: Response) => {
    try {
      const recommendAi = await this.recommendService.getCommentAI();
      if (!recommendAi) {
        res.status(400).json({ message: "comment not found" });
      } else {
        res.json(recommendAi);
      }
    } catch (error) {
      console.log("AI", error);
      res.status(500).json({ message: "AI error", error });
    }
  };

  getCommentDetail = async (req: Request, res: Response) => {
    try {
      const recommends = await this.recommendService.getCommentDetail();
      if (!recommends) {
        res.status(400).json({ message: "comment not found" });
      } else {
        res.json(recommends);
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "DB error", error });
    }
  };

  getCommentAndPredict = async (req: Request, res: Response) => {
    try {
      let comment = req.body.comment;

      let result = await axios.post("http://localhost:8000/", [comment]);

      console.log(result.data);

      res.status(200).json({ result: result.data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "DB error", error });
    }
  };

  // getCommentScore = async (req:Request, res:Response) => {
  //   try{
  //     const scores = await this.recommendService.getCommentSingleScore();
  //     res.json(scores);
  //   }catch(error){
  //     console.log(error);
  //     res.status(500).json({ message: "DB score error", error });
  //   }
  // }

  // chooseScore = async (req:Request, res: Response) => {
  //   try{
  //     console.log(req.body.score);
  //   }catch(error){
  //     console.log(error)
  //   }
  // }
}
// postCommentAI = async (req: Request, res: Response) => {
//   try {
//     const smartphoneId = parseInt(req.params.id);
//     if (isNaN(smartphoneId)) {
//       res.json({ message: "Invalid ID" });
//       return;
//     }
//     const smartphone = await this.recommendService.postCommentAI(
//       smartphoneId
//     );
//     res.json({ smartphone });
//   } catch (err) {
//     console.log(err);
//   }
// };
