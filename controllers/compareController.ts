import { CompareService } from "../services/compareService";
import type { Request, Response } from "express";

export class CompareController {
  constructor(private compareService: CompareService) {}

  getPhoneByBrandId = async (req: Request, res: Response) => {
    let userReq = req.body.brand;
    // console.log("received brand name:", userReq);
    let brandId;
    try {
      let brandRow = await this.compareService.getBrandIdByName(userReq);

      // console.log("got brand row:", brandRow);
      if (!brandRow) {
        res.status(400).json({ msg: "brand not found" });
      } else {
        brandId = brandRow.id;
        // console.log("brandId:", typeof brandId);
        const smartphone = await this.compareService.getPhoneByBrandId(brandId);
        res.json({ smartphone });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "internal server error" });
    }
  };

  getOnePhoneByBrandId = async (req: Request, res: Response) => {
    try {
      const eachSmartphone = await this.compareService.getPhoneByPhoneId();
      if (eachSmartphone.length === 0) {
        res.status(400).json({ message: "smartphone not found" });
      } else {
        res.json(eachSmartphone);
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
