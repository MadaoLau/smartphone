import env from "./env";
import express from "express";
// import { Request, Response } from "express";
// import fetch from "node-fetch";
import path from "path";
import { logger } from "./utils/logger";

import config from "./knexfile";
import Knex from "knex";

export const knex = Knex(config[env.NODE_ENV || "development"]);

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

import { routes } from "./routes/router";
app.use("/", routes);

app.use(express.static(path.join(__dirname, "public")));

app.use((req, res) => {
  res.sendFile(path.resolve("./public/404.html"));
});

app.listen(env.SERVER_PORT, () => {
  logger.info(`Listening to PORT: http://localhost:${env.SERVER_PORT}`);
});
