let displayPhoneArea = document.querySelector("#displayPhoneArea");
let phoneTemplate = document.querySelector("#displayPhoneArea li");
let searchBrand = document.querySelector("#searchBrand");
let thisPhoneBut = document.querySelector("#displayPhoneArea button");
// let compareTemplate = document.querySelector('#cd-products-columns .product')
let compare_1 = document.querySelector("#compare_1");
let compare_2 = document.querySelector("#compare_2");
let compare_3 = document.querySelector("#compare_3");

let delCompare_1 = document.querySelector("#delCompare_1");
let delCompare_2 = document.querySelector("#delCompare_2");
let delCompare_3 = document.querySelector("#delCompare_3");
// let compareInfo_1 = document.querySelector("#compare_1 li")
// let compareInfo_2 = document.querySelector("#compare_1 li")
// let compareInfo_3 = document.querySelector("#compare_1 li")

// window.onload = myMain;

// function myMain() {
//   document.getElementById("displayPhoneArea").onclick = clickedName;
// }

phoneTemplate.remove();

for (let i = 1; i < 4; i++) {
  document
    .getElementById("delCompare_" + i)
    .addEventListener("click", async () => {
      document.getElementById("_" + i + "img").src = "";
      document.getElementById("_" + i + "img").alt = "";
      //.......
    });
}

delCompare_1.addEventListener("click", async () => {
  // document.querySelector("#compare_1 img").src = ''
  //         document.querySelector("#compare_1 img").alt = ''
  document.getElementById("_1img").src = "";
  document.getElementById("_1img").alt = "";
  document.getElementById("_1name").textContent = "";
  document.getElementById("_1price").textContent = "";
  document.getElementById("_1screen_size").textContent = "";
  document.getElementById("_1resolution").textContent = "";
  document.getElementById("_1network").textContent = "";
  document.getElementById("_1os").textContent = "";
  document.getElementById("_1refresh_rate").textContent = "";
  document.getElementById("_1weight").textContent = "";
  document.getElementById("_1cpu").textContent = "";
  document.getElementById("_1ram").textContent = "";
  document.getElementById("_1functions").textContent = "";
  document.getElementById("_1quick_charge_rate").textContent = "";
  document.getElementById("_1wifi_generation").textContent = "";
  document.getElementById("_1bluetooth").textContent = "";
  document.getElementById("_1size").textContent = "";
  document.getElementById("_1front_cam").textContent = "";
  document.getElementById("_1main_cam").textContent = "";
  document.getElementById("_1battery").textContent = "";
  document.getElementById("_1storage").textContent = "";
  document.getElementById("_1release_date").textContent = "";
  document.getElementById("_1ip_code").textContent = "";
});

delCompare_2.addEventListener("click", async () => {
  // document.querySelector("#compare_2 img").src = ''
  // document.querySelector("#compare_2 img").alt = ''
  document.getElementById("_2img").src = "";
  document.getElementById("_2img").alt = "";

  document.getElementById("_2name").textContent = "";
  document.getElementById("_2price").textContent = "";
  document.getElementById("_2screen_size").textContent = "";
  document.getElementById("_2resolution").textContent = "";
  document.getElementById("_2network").textContent = "";
  document.getElementById("_2os").textContent = "";
  document.getElementById("_2refresh_rate").textContent = "";
  document.getElementById("_2weight").textContent = "";
  document.getElementById("_2cpu").textContent = "";
  document.getElementById("_2ram").textContent = "";
  document.getElementById("_2functions").textContent = "";
  document.getElementById("_2quick_charge_rate").textContent = "";
  document.getElementById("_2wifi_generation").textContent = "";
  document.getElementById("_2bluetooth").textContent = "";
  document.getElementById("_2size").textContent = "";
  document.getElementById("_2front_cam").textContent = "";
  document.getElementById("_2main_cam").textContent = "";
  document.getElementById("_2battery").textContent = "";
  document.getElementById("_2storage").textContent = "";
  document.getElementById("_2release_date").textContent = "";
  document.getElementById("_2ip_code").textContent = "";
});

delCompare_3.addEventListener("click", async () => {
  // document.querySelector("#compare_3 img").src = ''
  //         document.querySelector("#compare_3 img").alt = ''
  document.getElementById("_3img").src = "";
  document.getElementById("_3img").alt = "";
  document.getElementById("_3name").textContent = "";
  document.getElementById("_3price").textContent = "";
  document.getElementById("_3screen_size").textContent = "";
  document.getElementById("_3resolution").textContent = "";
  document.getElementById("_3network").textContent = "";
  document.getElementById("_3os").textContent = "";
  document.getElementById("_3refresh_rate").textContent = "";
  document.getElementById("_3weight").textContent = "";
  document.getElementById("_3cpu").textContent = "";
  document.getElementById("_3ram").textContent = "";
  document.getElementById("_3functions").textContent = "";
  document.getElementById("_3quick_charge_rate").textContent = "";
  document.getElementById("_3wifi_generation").textContent = "";
  document.getElementById("_3bluetooth").textContent = "";
  document.getElementById("_3size").textContent = "";
  document.getElementById("_3front_cam").textContent = "";
  document.getElementById("_3main_cam").textContent = "";
  document.getElementById("_3battery").textContent = "";
  document.getElementById("_3storage").textContent = "";
  document.getElementById("_3release_date").textContent = "";
  document.getElementById("_3ip_code").textContent = "";
});
async function loadSearchByBrand() {
  let smartphones;
  searchBrand.addEventListener("submit", async (e) => {
    e.preventDefault();
    displayPhoneArea.innerHTML = ``;
    let form = e.target;
    body = {
      brand: form.brand.value,
    };
    // console.log("send searchBrand value:", body);
    try {
      let res = await fetch("/compare", {
        method: "post",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      });

      let json = await res.json();
      smartphones = json.smartphone;
      // console.log("All selected smartphones:", smartphones);

      for (let i = 0; i < smartphones.length; i++) {
        let phoneItem = phoneTemplate.cloneNode(true);
        // let thisPhoneDiv = document.createElement("Button");
        // thisPhoneBut.setAttribute("class", `phoneNo${i}`);
        let a = document.createElement("a");
        a.setAttribute("href", `smartphoneInfo.html?id=${smartphones[i].id}`);
        phoneItem.querySelector(`.phName`).textContent = smartphones[i].name;
        phoneItem.querySelector(
          `Button`
        ).className = `btn btn-primary phoneNo${i} ${smartphones[i].name}`;
        phoneItem.querySelector(".phImg").src =
          "https://www.price.com.hk" + `${smartphones[i].image}`;
        phoneItem.querySelector(
          "a"
        ).href = `smartphoneInfo.html?id=${smartphones[i].id}`;
        displayPhoneArea.appendChild(phoneItem);
      }
    } catch (error) {
      console.log("input all selected smartphones fail", error);
    }
  });
  document.getElementById("displayPhoneArea").onclick = clickedName;

  async function clickedName(e) {
    if (e.target.tagName == "BUTTON") {
      let phone = e.target.className;
      clickedPhone = phone.split(" ").slice(3).join(" ");

      // console.log("wtttttttt?", clickedPhone);
      for (let i = 0; i < smartphones.length; i++) {
        if (smartphones[i].name == clickedPhone) {
          clickedPhone = smartphones[i];
        }
      }
      
      if (document.getElementById("_1name").textContent == "") {
        // document.querySelector("#compare_1 img").src = 'https://www.price.com.hk/' + clickedPhone.image
        // document.querySelector("#compare_1 img").alt = clickedPhone.name
        document.getElementById("_1img").src =
          "https://www.price.com.hk/" + clickedPhone.image;
        document.getElementById("_1img").alt = clickedPhone.name;
        document.getElementById("_1name").textContent = clickedPhone.name;
        document.getElementById("_1price").textContent = clickedPhone.price;
        document.getElementById("_1screen_size").textContent =
          clickedPhone.screen_size;
        document.getElementById("_1resolution").textContent =
          clickedPhone.resolution;
        document.getElementById("_1network").textContent = clickedPhone.network;
        document.getElementById("_1os").textContent = clickedPhone.os;
        document.getElementById("_1refresh_rate").textContent =
          clickedPhone.refresh_rate;
        document.getElementById("_1weight").textContent = clickedPhone.weight;
        document.getElementById("_1cpu").textContent = clickedPhone.cpu;
        document.getElementById("_1ram").textContent = clickedPhone.ram;
        document.getElementById("_1functions").textContent =
          clickedPhone.functions;
        document.getElementById("_1quick_charge_rate").textContent =
          clickedPhone.quick_charge_rate;
        document.getElementById("_1wifi_generation").textContent =
          clickedPhone.wifi_generation;
        document.getElementById("_1bluetooth").textContent =
          clickedPhone.bluetooth;
        document.getElementById("_1size").textContent = clickedPhone.size;
        document.getElementById("_1front_cam").textContent =
          clickedPhone.front_cam;
        document.getElementById("_1main_cam").textContent =
          clickedPhone.main_cam;
        document.getElementById("_1battery").textContent = clickedPhone.battery;
        document.getElementById("_1storage").textContent = clickedPhone.storage;
        document.getElementById("_1release_date").textContent =
          clickedPhone.release_date;
        document.getElementById("_1ip_code").textContent = clickedPhone.ip_code;
        // compare_1.innerHTML = /* html */ `

        // <div class="top-info">
        //           <img src="https://www.price.com.hk/${clickedPhone.image}" alt="product image" />
        //         </div>
        //         <ul class="cd-features-list">
        //           <li class="_1name">${clickedPhone.name}</li>
        //           <li class="_1price">${clickedPhone.price}</li>
        //           <li class="_1screen_size">${clickedPhone.screen_size}</li>
        //           <li class="_1resolution">${clickedPhone.resolution}</li>
        //           <li class="_1network">${clickedPhone.network}</li>
        //           <li class="_1os">${clickedPhone.os}</li>
        //           <li class="_1refresh_rate">${clickedPhone.refresh_rate}</li>
        //           <li class="_1weight">${clickedPhone.weight}</li>
        //           <li class="_1cpu">${clickedPhone.cpu}</li>
        //           <li class="_1ram">${clickedPhone.ram}</li>
        //           <li class="_1functions">${clickedPhone.functions}</li>
        //           <li class="_1quick_charge_rate">${clickedPhone.quick_charge_rate}</li>
        //           <li class="_1wifi_generation">${clickedPhone.wifi_generation}</li>
        //           <li class="_1bluetooth">${clickedPhone.bluetooth}</li>
        //           <li class="_1size">${clickedPhone.size}</li>
        //           <li class="_1front_cam">${clickedPhone.front_cam}</li>
        //           <li class="_1main_cam">${clickedPhone.main_cam}</li>
        //           <li class="_1battery">${clickedPhone.battery}</li>
        //           <li class="_1storage">${clickedPhone.storage}</li>
        //           <li class="_1release_date">${clickedPhone.release_date}</li>
        //           <li class="_1ip_code">${clickedPhone.ip_code}</li>
        //         </ul>
        // `
      } else if (document.getElementById("_2name").textContent == "") {
        // document.querySelector("#compare_2 img").src = 'https://www.price.com.hk/' + clickedPhone.image
        // document.querySelector("#compare_2 img").alt = clickedPhone.name
        document.getElementById("_2img").src =
          "https://www.price.com.hk/" + clickedPhone.image;
        document.getElementById("_2img").alt = clickedPhone.name;
        document.getElementById("_2name").textContent = clickedPhone.name;
        document.getElementById("_2price").textContent = clickedPhone.price;
        document.getElementById("_2screen_size").textContent =
          clickedPhone.screen_size;
        document.getElementById("_2resolution").textContent =
          clickedPhone.resolution;
        document.getElementById("_2network").textContent = clickedPhone.network;
        document.getElementById("_2os").textContent = clickedPhone.os;
        document.getElementById("_2refresh_rate").textContent =
          clickedPhone.refresh_rate;
        document.getElementById("_2weight").textContent = clickedPhone.weight;
        document.getElementById("_2cpu").textContent = clickedPhone.cpu;
        document.getElementById("_2ram").textContent = clickedPhone.ram;
        document.getElementById("_2functions").textContent =
          clickedPhone.functions;
        document.getElementById("_2quick_charge_rate").textContent =
          clickedPhone.quick_charge_rate;
        document.getElementById("_2wifi_generation").textContent =
          clickedPhone.wifi_generation;
        document.getElementById("_2bluetooth").textContent =
          clickedPhone.bluetooth;
        document.getElementById("_2size").textContent = clickedPhone.size;
        document.getElementById("_2front_cam").textContent =
          clickedPhone.front_cam;
        document.getElementById("_2main_cam").textContent =
          clickedPhone.main_cam;
        document.getElementById("_2battery").textContent = clickedPhone.battery;
        document.getElementById("_2storage").textContent = clickedPhone.storage;
        document.getElementById("_2release_date").textContent =
          clickedPhone.refresh_rate;
        document.getElementById("_2ip_code").textContent = clickedPhone.ip_code;
      } else if (document.getElementById("_3name").textContent == "") {
        // document.querySelector("#compare_3 img").src = 'https://www.price.com.hk/' + clickedPhone.image
        // document.querySelector("#compare_3 img").alt = clickedPhone.name
        document.getElementById("_3img").src =
          "https://www.price.com.hk/" + clickedPhone.image;
        document.getElementById("_3img").alt = clickedPhone.name;
        document.getElementById("_3name").textContent = clickedPhone.name;
        document.getElementById("_3price").textContent = clickedPhone.price;
        document.getElementById("_3screen_size").textContent =
          clickedPhone.screen_size;
        document.getElementById("_3resolution").textContent =
          clickedPhone.resolution;
        document.getElementById("_3network").textContent = clickedPhone.network;
        document.getElementById("_3os").textContent = clickedPhone.os;
        document.getElementById("_3refresh_rate").textContent =
          clickedPhone.refresh_rate;
        document.getElementById("_3weight").textContent = clickedPhone.weight;
        document.getElementById("_3cpu").textContent = clickedPhone.cpu;
        document.getElementById("_3ram").textContent = clickedPhone.ram;
        document.getElementById("_3functions").textContent =
          clickedPhone.functions;
        document.getElementById("_3quick_charge_rate").textContent =
          clickedPhone.quick_charge_rate;
        document.getElementById("_3wifi_generation").textContent =
          clickedPhone.wifi_generation;
        document.getElementById("_3bluetooth").textContent =
          clickedPhone.bluetooth;
        document.getElementById("_3size").textContent = clickedPhone.size;
        document.getElementById("_3front_cam").textContent =
          clickedPhone.front_cam;
        document.getElementById("_3main_cam").textContent =
          clickedPhone.main_cam;
        document.getElementById("_3battery").textContent = clickedPhone.battery;
        document.getElementById("_3storage").textContent = clickedPhone.storage;
        document.getElementById("_3release_date").textContent =
          clickedPhone.refresh_rate;
        document.getElementById("_3ip_code").textContent = clickedPhone.ip_code;
      } else {
        console.log("no compare area left");
      }
    }
  }
}
loadSearchByBrand();

// async function clickedName(e) {
//   if (e.target.tagName == 'BUTTON') {
//     let phone =  e.target.className
//     clickedPhone = phone.substr(phone.indexOf(" ")+1)
//     // alert(e.target.textContent);
//   }

//   clickedPhone = {
//     body: clickedPhone
//   }
//   console.log('wtttttttt?',clickedPhone);

//   try {
//     let res = await fetch('/compare',{
//       method: 'post',
//       headers: {
//         "Content-Type": "application/json",
//       },
//       body: JSON.stringify(clickedPhone)
//     })

//     let json = await res.json()
//     console.log('clickedPhone received from server:', json);

//   } catch (error) {
//     console.log('send clickedPhone fail', error);
//   }
// }

// document.onclick = function(){
//   let obj = event.srcElement
// }

// async function addToCompare() {
//   searchBrand.addEventListener("submit", async (e) => {
//     let
//     e.preventDefault();
//     displayPhoneArea.innerHtml = "";
//     // let value = brandOpt.options[brandOpt.selectedIndex].value;
//     let form = e.target;
//     body = {
//       brand: form.brand.value,
//     };
//     console.log("send searchBrand value:", body);
//     try {
//       let res = await fetch("/compare", {
//         method: "post",
//         headers: {
//           "Content-Type": "application/json",
//         },
//         body: JSON.stringify(body),
//       });

//       let json = await res.json();
//       let smartphones = json.smartphone;
//       console.log("what is the json?", smartphones);

//       for (let i = 2; i < smartphones.length; i++) {
//         let phoneItem = phoneTemplate.cloneNode(true);
//         let thisPhoneDiv = document.createElement("Button");
//         thisPhoneDiv.setAttribute("class", `phoneNo${i}`);
//         phoneItem.querySelector(".phName").textContent = smartphones[i].name;
//         // phoneItem.querySelector(".price").textContent =
//         //   "$HKD: " + smartphones[i].price;
//         phoneItem.querySelector(".phImg").src = 'https://www.price.com.hk'+`${smartphones[i].image}`;
//         phoneTemplate.querySelector(`Button`).className = `phoneNo${i}`
//         displayPhoneArea.appendChild(phoneItem);
//       }
//       form.reset();
//     } catch (error) {
//       console.log(error);
//     }
//   });
// }
// loadSearchByBrand();
