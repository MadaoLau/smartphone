let searchParams = new URLSearchParams(location.search);
let id = searchParams.get("id");

let commentList = document.querySelector(".comment-list-zone");
const commentZoneArea = (comment) => {
  let div = document.createElement("div");
  // div.className = "col-md-6";
  div.innerHTML = /* html */ `
    <div class="comment-zone-phone">
    <h3 class="comment-name">${comment.name}</h3><br>
    <p class="comment">評價1：${comment.cm1}</p><br>
    <p class="comment">評價2：${comment.cm2}</p><br>
    <p class="comment">評價3：${comment.cm3}</p>
    </div>
    `;
  commentList.appendChild(div);
};

async function loadCommentZone() {
  let res = await fetch("/recommends");
  let json = await res.json();
  // console.log(json);
  commentList.innerHTML = "";
  for (let recommendHot of json) {
    if (recommendHot.name != "") {
      if (recommendHot.id == id) {
        commentZoneArea(recommendHot);
      }
    }
  }
}

async function loadCommentScore() {
  let res = await fetch("/recommends/score");
  let json = await res.json();
  // console.log("score:", json);
  //TODO: Show single score for each comment
  // for (let score of json) {
  //     if(score.comment == ){
  //     commentZoneArea(recommendHot);
  //   }
  // }
}

window.onload = async function () {
  await loadCommentZone();
  await loadCommentScore();
};
