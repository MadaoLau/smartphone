let searchParams = new URLSearchParams(location.search)
let id = searchParams.get("id")

let smartphoneList = document.querySelector(".queryClass")
const smartphoneArea = (smartphone)=>{
    document.querySelector(".photo img").src = 'https://www.price.com.hk/' + smartphone.image
          document.querySelector(".photo img").alt = smartphone.name
          document.getElementById("_1name").textContent = smartphone.name
          document.getElementById("_1price").textContent =smartphone.price
          document.getElementById("_1screen_size").textContent =smartphone.screen_size
          document.getElementById("_1resolution").textContent =smartphone.resolution
          document.getElementById("_1network").textContent =smartphone.network
          document.getElementById("_1os").textContent =smartphone.os
          document.getElementById("_1refresh_rate").textContent =smartphone.refresh_rate
          document.getElementById("_1weight").textContent =smartphone.weight
          document.getElementById("_1cpu").textContent =smartphone.cpu
          document.getElementById("_1ram").textContent =smartphone.ram
          document.getElementById("_1functions").textContent =smartphone.functions
          document.getElementById("_1quick_charge_rate").textContent =smartphone.quick_charge_rate
          document.getElementById("_1wifi_generation").textContent =smartphone.wifi_generation
          document.getElementById("_1bluetooth").textContent =smartphone.bluetooth
          document.getElementById("_1size").textContent =smartphone.size
          document.getElementById("_1front_cam").textContent =smartphone.front_cam
          document.getElementById("_1main_cam").textContent =smartphone.main_cam
          document.getElementById("_1battery").textContent =smartphone.battery
          document.getElementById("_1storage").textContent =smartphone.storage
          document.getElementById("_1release_date").textContent =smartphone.release_date
          document.getElementById("_1ip_code").textContent =smartphone.ip_code
    // let div = document.createElement('div')
    // div.className = "col-md-12"
    // div.innerHTML = /*html*/`
    // <ul class="cd-features-list">
    //    <li>價錢: ${smartphone.price}</li>
    //    <li>螢幕尺寸: ${smartphone.screen_size} </li>
    //    <li>解像度: ${smartphone.resolution}</li>
    //    <li>網絡制式: ${smartphone.network}</li>
    //    <li>作業系統: ${smartphone.os}</li>
    //    <li>螢幕刷新率: ${smartphone.refresh_rate}</li>
    //    <li>重量: ${smartphone.weight}</li>
    //    <li>處理器: ${smartphone.cpu}</li>
    //    <li>記憶體: ${smartphone.ram}</li>
    //    <li>功能: ${smartphone.functions}</li>
    //    <li>快充功率: ${smartphone.quick_charge_rate}</li>
    //    <li>Wi-Fi制式: ${smartphone.wifi_generation}</li>
    //    <li>藍牙版本: ${smartphone.bluetooth}</li>
    //    <li>尺寸: ${smartphone.size}</li>
    //    <li>前鏡頭: ${smartphone.front_cam}</li>
    //    <li>後鏡頭: ${smartphone.main_cam}</li>
    //    <li>電池容量: ${smartphone.battery}</li>
    //    <li>容量: ${smartphone.storage}</li>
    //    <li>上市日期: ${smartphone.release_date}</li>
    //    <li>防塵/防水等級: ${smartphone.price}</li>
    //  </ul>`;
    // smartphoneList.appendChild(div)
}

async function loadSmartphone(){
    let res = await fetch('/compare')
    let json = await res.json()
    // console.log('json--------', json);

    document.querySelector(".photo img").src = ''
          document.querySelector(".photo img").alt = ''
          document.getElementById("_1name").textContent = ''
          document.getElementById("_1price").textContent = ''
          document.getElementById("_1screen_size").textContent = ''
          document.getElementById("_1resolution").textContent = ''
          document.getElementById("_1network").textContent = ''
          document.getElementById("_1os").textContent = ''
          document.getElementById("_1refresh_rate").textContent = ''
          document.getElementById("_1weight").textContent = ''
          document.getElementById("_1cpu").textContent = ''
          document.getElementById("_1ram").textContent = ''
          document.getElementById("_1functions").textContent = ''
          document.getElementById("_1quick_charge_rate").textContent = ''
          document.getElementById("_1wifi_generation").textContent = ''
          document.getElementById("_1bluetooth").textContent = ''
          document.getElementById("_1size").textContent = ''
          document.getElementById("_1front_cam").textContent = ''
          document.getElementById("_1main_cam").textContent = ''
          document.getElementById("_1battery").textContent = ''
          document.getElementById("_1storage").textContent = ''
          document.getElementById("_1release_date").textContent = ''
          document.getElementById("_1ip_code").textContent = ''
    for(let eachSmartphone of json){
        if(eachSmartphone.id == id){
            smartphoneArea(eachSmartphone)
        }
    }
}


window.onload = async function(){
    await loadSmartphone()
}