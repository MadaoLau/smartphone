// let brandZoneList = document.querySelector(".recommend-brand-zone");
// const brandZoneArea = (recommend) => {
//   let div = document.createElement("div");
//   div.className = "col-md-2";
//   div.innerHTML = /* html */ `
//     <div class="recommend-brand-zone-phone">
//     <h3 class="recommend-bdname">${recommend.bdname}</h3>
//     <p class="good-comment">好評<i class="far fa-thumbs-up"></i> ${recommend.positive}</p>
//     <p class="bad-comment">負評<i class="far fa-thumbs-down"></i> ${recommend.negative}</p>
//   </div>
//     `;
//   brandZoneList.appendChild(div);
// };


let brandZoneList = document.querySelector(".recommend-brand-zone");
const brandZoneArea = (recommend) => {
  let div = document.createElement("tr");
  // div.className = "col-md-2";
  div.innerHTML = /* html */ `
  <td>${recommend.bdname}</td>
  <td class="good-comment">${recommend.positive}</td>
  <td class="bad-comment">${recommend.negative}</td>
  <td>${recommend.point}</td>
  `;
  brandZoneList.appendChild(div);

  //console.log(document.getElementsByClassName("bad-comment"));
  //console.log(document.getElementById("goodd" + num));
  // let num = 0;
  //id=${"goodd" + ++num}
  //Reemo code review example
  // document.getElementById("goodd" + num).style.display = "none";
};

// let hotZoneList = document.querySelector(".recommend-hot-zone");
// const hotZoneArea = (recommendHot) => {
//   let div = document.createElement("div");
//   div.className = "col-md-3";
//   div.innerHTML = /* html */ `
//     <div class="recommend-hot-zone-phone">
//     <h4 class="recommend-phone-name">${recommendHot.name}</h4>
//     <p class="score">分數:${recommendHot.totalScore.toFixed(2)}</p>
//     <a href=comment.html?id=${recommendHot.id}>詳細評論</a>
//   </div>
//     `;
//   hotZoneList.appendChild(div);
// };
let hotZoneList = document.querySelector(".recommend-hot-zone");
const hotZoneArea = (recommendHot) => {
  let div = document.createElement("tr");
  // div.className = "col-md-3";
  div.innerHTML = /* html */ `
    <td class="recommend-phone-name">${recommendHot.name}</td>
    <td class="score">${recommendHot.totalScore.toFixed(2)}</td>
    <td><a href=comment.html?id=${recommendHot.id}>按此</a></td>
    `;
  hotZoneList.appendChild(div);
};

// function showComment(a,b,c){
//   hotZoneList.innerHTML = "";
//   loadHotZone(99,a);
//   b.style.display = "none"
//   c.style.display = "block"
// }
async function loadBrandZone() {
  let res = await fetch("/recommends/ai");
  let json = await res.json();
  // console.log(json);
  brandZoneList.innerHTML = "";
  for (let recommend of json) {
    brandZoneArea(recommend);
  }
}

async function loadHotZone(x, y) {
  let res = await fetch("/recommends");
  let json = await res.json();
  // console.log(json);
  hotZoneList.innerHTML = "";
  for (let i = 0; i < json.length; i++) {
    if (json[i].name !== "") {
      if (json[i].totalScore >= x && json[i].totalScore <= y) {
        hotZoneArea(json[i]);
      }
    }
  }
}

//separate by score level and choose button
let score100 = document.querySelector(`#score100`);
score100.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(99.9, 100);
});
let score99 = document.querySelector(`#score99`);
score99.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(99, 99.89);
});
let score90 = document.querySelector(`#score90`);
score90.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(90, 98);
});
let score80 = document.querySelector(`#score80`);
score80.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(80, 89);
});
let score70 = document.querySelector(`#score70`);
score70.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(70, 79);
});
let score60 = document.querySelector(`#score60`);
score60.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(60, 69);
});
let score50 = document.querySelector(`#score50`);
score50.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(50, 59);
});
let score40 = document.querySelector(`#score40`);
score40.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(40, 49);
});
let score30 = document.querySelector(`#score30`);
score30.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(30, 39);
});
let score20 = document.querySelector(`#score20`);
score20.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(20, 29);
});
let score10 = document.querySelector(`#score10`);
score10.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(10, 19);
});
let score0 = document.querySelector(`#score0`);
score0.addEventListener("click", (e) => {
  hotZoneList.innerHTML = "";
  loadHotZone(0, 10);
});

window.onload = async function () {
  await loadBrandZone();
  await loadHotZone(99.9, 100);
};
