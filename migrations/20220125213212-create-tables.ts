import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  if (!(await knex.schema.hasTable("brand"))) {
    await knex.schema.createTable("brand", (table) => {
      table.increments("id");
      table.string("name").notNullable();
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("smartphone"))) {
    await knex.schema.createTable("smartphone", (table) => {
      table.increments("id");
      table.string("name").notNullable();
      table.integer("price").notNullable();
      table.string("image").nullable();
      table.string("screen_size").nullable();
      table.string("resolution").nullable();
      table.string("network").nullable();
      table.string("os").nullable();
      table.string("refresh_rate").nullable();
      table.string("weight").nullable();
      table.string("cpu").nullable();
      table.string("ram").nullable();
      table.string("functions").nullable();
      table.string("quick_charge_rate").nullable();
      table.string("wifi_generation").nullable();
      table.string("bluetooth").nullable();
      table.string("size").nullable();
      table.string("front_cam").nullable();
      table.string("main_cam").nullable();
      table.string("battery").nullable();
      table.string("color").nullable();
      table.string("storage").nullable();
      table.string("storage_card").nullable();
      table.string("release_date").nullable();
      table.boolean("multi_sim_card").nullable();
      table.boolean("earphone_plug").nullable();
      table.string("ip_code").nullable();
      table.integer("brand_id").notNullable().references("brand.id");
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("comment"))) {
    await knex.schema.createTable("comment", (table) => {
      table.increments("id");
      table.text("cm1").nullable();
      table.text("cm2").nullable();
      table.text("cm3").nullable();
      table.string("name").notNullable();
      table.integer("marks").notNullable();
      table.integer("brand_id").nullable().references("brand.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("comment");
  await knex.schema.dropTableIfExists("smartphone");
  await knex.schema.dropTableIfExists("brand");
}
