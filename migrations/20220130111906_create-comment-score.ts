import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  if (!(await knex.schema.hasTable("comment_ai_score"))) {
    await knex.schema.createTable("comment_ai_score", (table) => {
      table.increments("id");
      table.text("comment").nullable();
      table.float("score").nullable();
      table.boolean("pos").nullable();
      table.integer("comment_id").nullable().references("comment.id");
      //   table.integer("brand_id").nullable().references("brand.id");
      //   table.integer("smartphone_id").nullable().references("smartphone.id");
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("comment_ai_score");
}
