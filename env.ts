import dotenv from "dotenv";
dotenv.config();

const env = {
  DB_NAME: process.env.DB_NAME || "smartphone",
  DB_USER: process.env.DB_USER || "smartphone",
  DB_PASSWORD: process.env.DB_PASSWORD || "smartphone",
  TEST_DB_NAME: process.env.TEST_DB_NAME || "smartphone_test",
  TEST_DB_USER: process.env.TEST_DB_USER || "smartphone_test",
  TEST_DB_PASSWORD: process.env.TEST_DB_PASSWORD || "smartphone_test",
  SERVER_PORT: process.env.SERVER_PORT || 3333,
  NODE_ENV: process.env.NODE_ENV || "development",
  POSTGRES_HOST: process.env.POSTGRES_HOST,
  POSTGRES_DB_NAME: process.env.POSTGRES_DB_NAME,
  POSTGRES_USER: process.env.POSTGRES_USER,
  POSTGRES_PASSWORD: process.env.POSTGRES_PASSWORD,
};

export default env;
